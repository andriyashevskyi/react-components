## About

Using React Context API, HOC and Compound Components 

## Dependencies

1. prop-types: ^15.6.1
2. react: ^16.4.0
3. react-dom: "^16.4.0
4. react-icons: "^2.2.7
5. react-scripts: "1.1.4


---

## Run

**npm install**

**npm start**

