import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AudioPlayerContext} from "./index";

const propTypes = {
    duration: PropTypes.number,
    currentTime: PropTypes.number,
    isLoaded: PropTypes.bool,
    setAudioTime: PropTypes.func,
    barClass: PropTypes.string,
    progressClass: PropTypes.string
};

const defaultProps = {
    barClass: 'progress-bar',
    progressClass: 'progress'
};


class Bar extends Component {
    constructor(props) {
        super(props);
        this.refProgress = React.createRef();
    }

    render() {
        const {
            duration,
            currentTime,
            isLoaded,
            setAudioTime,
            barClass = 'progress-bar',
            progressClass = 'progress'
        } = this.props;
        const width = isLoaded ? currentTime / duration : 0;
        return (
            <div
                className={progressClass}
                ref={this.refProgress}
                onClick={(event) => {
                    const rect = this.refProgress.current.getBoundingClientRect(),
                        left = event.clientX - rect.left,
                        multiply = left / rect.width,
                        time = this.props.duration * multiply;
                    setAudioTime(time);


                }}>
                <div className={barClass} style={{
                    width: `${width * 100}%`
                }}
                >
                </div>
            </div>
        )
    }
}

Bar.propTypes = propTypes;
Bar.defaultProps = defaultProps;

const WithConsumer = (Comp) => {
    return function WrappedComponent(props) {
        return (
            <AudioPlayerContext.Consumer>
                {(state) => (<Comp {...props} {...state}/>)}
            </AudioPlayerContext.Consumer>
        )

    }
};
const ProgressBar = WithConsumer(Bar);
export default ProgressBar;