import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AudioPlayerContext} from "./index";

const propTypes = {
    buttonClass: PropTypes.string,
};

const defaultProps = {
    text: 'icon-button'
};


class Play extends Component {
    render() {
        const {buttonClass = 'icon-button'} = this.props;
        return (
            <AudioPlayerContext.Consumer>
                {(state) => (
                    <button
                        className={state.isPlaying ? `active ${buttonClass}` : `${buttonClass}`}
                        onClick={state.play}
                        disabled={state.isPlaying}
                        title="play"
                    >
                        {this.props.children}
                    </button>
                )}
            </AudioPlayerContext.Consumer>
        )
    }
}

Play.propTypes = propTypes;
Play.defaultProps = defaultProps;

export default Play;