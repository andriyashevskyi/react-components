import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AudioPlayerContext} from "./index";

const propTypes = {
    buttonClass: PropTypes.string
};

const defaultProps = {
    buttonClass: 'radio-button'
};


class Next extends Component {
    render() {
        const {buttonClass = 'radio-button'} = this.props;
        return (
            <AudioPlayerContext.Consumer>
                {(state) => <button
                    className={buttonClass}
                    onClick={state.next}
                    title="Next">{this.props.children}</button>}
            </AudioPlayerContext.Consumer>
        )
    }
}

Next.propTypes = propTypes;
Next.defaultProps = defaultProps;

export default Next;