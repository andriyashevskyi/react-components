import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TrackName from "./trackName";
import './index.css';

const propTypes = {
    playerClassName: PropTypes.string,
    source: PropTypes.array.isRequired,
    trackNameEnabled: PropTypes.bool,
    headerClass: PropTypes.string
};


const defaultProps = {
    playerClassName: 'player',
    trackNameEnabled: true
};

export const AudioPlayerContext = React.createContext();

export class AudioPlayer extends Component {
    constructor(props) {
        super(props);
        this.audioRef = React.createRef();
    }

    play = () => {
        this.audioRef.current.play();
        this.setState({
            isPlaying: true
        })
    };
    pause = () => {
        this.audioRef.current.pause();
        this.setState({
            isPlaying: false
        })
    };

    loadedHandler = () => (
        this.setState({
            isLoaded: true,
            duration: this.audioRef.current.duration
        })
    );
    timeUpdateHandler = () => (
        this.setState({
            currentTime: this.audioRef.current.currentTime
        })
    );

    setAudioTime = (time) => (
        this.audioRef.current.currentTime = time

    );

    endedHandler = () => {
        this.setState({
            isPlaying: false
        });
        this.nextTrack();

    };
    nextTrack = () => {
        if (this.state.activeTrack !== this.state.tracks - 1) {
            this.setState((prevState) => ({
                    activeTrack: prevState.activeTrack + 1,

                })
            );

            if (this.state.isPlaying) {
                this.setState({
                    isPlaying: false
                }, () => (this.play()));
            }

        }
        else {
            return null;
        }


    };

    prevTrack = () => {
        if (this.state.activeTrack !== 0) {
            this.setState((prevState) => ({
                    activeTrack: prevState.activeTrack - 1
                })
            );
            if (this.state.isPlaying) {
                this.setState({
                    isPlaying: false
                }, () => (this.play()));
            }

        }
        else {
            return null;
        }
    };


    state = {
        isPlaying: false,
        isLoaded: false,
        duration: null,
        currentTime: null,
        play: this.play,
        pause: this.pause,
        next: this.nextTrack,
        prev: this.prevTrack,
        loadedHandler: this.loadedHandler,
        timeUpdateHandler: this.timeUpdateHandler,
        setAudioTime: this.setAudioTime,
        endedHandler: this.endedHandler,
        activeTrack: 0,
        tracks: this.props.source.length

    };

    render() {
        const {playerClassName, source, trackNameEnabled, headerClass} = this.props;
        const {timeUpdateHandler, loadedHandler, endedHandler, activeTrack} = this.state;

        return (
            <AudioPlayerContext.Provider value={this.state}>
                <div className={playerClassName}>
                    <audio src={source[activeTrack].data}
                           ref={this.audioRef}
                           onTimeUpdate={timeUpdateHandler}
                           onLoadedData={loadedHandler}
                           onEnded={endedHandler}/>
                    {trackNameEnabled
                        ? (<TrackName headerClass={headerClass}>{source[activeTrack].name}</TrackName>)
                        : null}
                    {this.props.children}
                </div>
            </AudioPlayerContext.Provider>)
    }
}

AudioPlayer.propTypes = propTypes;
AudioPlayer.defaultProps = defaultProps;
