import React, {Component} from 'react';

class TrackName extends Component {
    render() {
        const {headerClass} = this.props;
        return (
            <h3 className={headerClass}>{this.props.children}</h3>
        )
    }
}

export default TrackName;