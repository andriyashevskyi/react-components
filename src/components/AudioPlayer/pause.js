import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AudioPlayerContext} from "./index";

const propTypes = {
    buttonClass: PropTypes.string

};

const defaultProps = {
    buttonClass: 'icon-button'
};


class Pause extends Component {
    render() {
        const {buttonClass = 'icon-button'} = this.props;
        return (
            <AudioPlayerContext.Consumer>
                {(state) => (<button
                    className={!state.isPlaying ? `active ${buttonClass}` : `${buttonClass}`}
                    onClick={state.pause}
                    disabled={!state.isPlaying}
                    title="pause"
                >
                    {this.props.children}
                </button>)}
            </AudioPlayerContext.Consumer>
        )
    }
}

Pause.propTypes = propTypes;
Pause.defaultProps = defaultProps;

export default Pause;