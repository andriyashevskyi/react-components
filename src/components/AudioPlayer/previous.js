import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AudioPlayerContext} from "./index";

const propTypes = {
    buttonClass: PropTypes.string
};

const defaultProps = {
    buttonClass: 'radio-button'
};


class Previous extends Component {
    render() {
        const {buttonClass = 'radio-button'} = this.props;
        return (
            <AudioPlayerContext.Consumer>
                {(state) => <button
                    className={buttonClass}
                    onClick={state.prev}
                    title="Prev">
                    {this.props.children}</button>}
            </AudioPlayerContext.Consumer>
        )
    }

}

Previous.propTypes = propTypes;
Previous.defaultProps = defaultProps;

export default Previous;