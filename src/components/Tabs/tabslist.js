import React, {Component} from 'react';
import {TabsContext} from "./index";
import PropTypes from 'prop-types';

const propTypes = {
    activeTab: PropTypes.number.isRequired,
    setActiveTab: PropTypes.func.isRequired,
    tabsHeaderClass: PropTypes.string
};


class TabsListWithoutContext extends Component {
    render() {
        const {activeTab, setActiveTab, tabsHeaderClass} = this.props;
        const children = React.Children.map(this.props.children,
            (child, index) => React.cloneElement(child, ({
                    isActive: index === activeTab,
                    onSelect: () => setActiveTab(index)
                })
            )
        );
        return (
            <div className={tabsHeaderClass}>{children}</div>

        )
    }
}

TabsListWithoutContext.propTypes = propTypes;

const withContext = (Comp) => {
    return function WithContextComponent(props) {
        return (
            <TabsContext.Consumer>

                {(state) => (<Comp {...state} {...props}/>)}

            </TabsContext.Consumer>
        )
    }
};

export const TabsList = withContext(TabsListWithoutContext);


