import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './index.css';

const propTypes = {
    tabClass: PropTypes.string
};

const defaultProps = {
    tabClass: 'tabs-container'
};

export const TabsContext = React.createContext();

export class TabsComponent extends Component {
    setTab = (index) => (this.setState({
        activeTab: index
    }));

    state = {
        activeTab: 0,
        setActiveTab: this.setTab
    };

    render() {
        const {tabsClass = 'tabs-container'} = this.props;
        return (
            <TabsContext.Provider value={this.state}>
                <div className={tabsClass}>
                    {this.props.children}
                </div>
            </TabsContext.Provider>
        )
    }
}

TabsComponent.propTypes = propTypes;
TabsComponent.defaultProps = defaultProps;

