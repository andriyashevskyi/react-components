import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propsTypes = {
    panelClass: PropTypes.string
};

const defaultProps = {
    panelClass: 'panel'
};

class Panel extends Component {
    render() {
        const {panelClass} = this.props;
        return (
            <div className={panelClass}>{this.props.children}</div>
        )
    }
}

Panel.propTypes = propsTypes;
Panel.defaultProps = defaultProps;

export default Panel;