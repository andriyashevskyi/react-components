import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    isActive: PropTypes.bool,
    onSelect: PropTypes.func,
    isDisabled: PropTypes.bool
};


class Tabs extends Component {
    render() {
        const {isActive, onSelect, isDisabled} = this.props;

        return (
            <div
                className={
                    isDisabled ? 'tab disabled'
                        : isActive ? 'tab active' : 'tab'
                }
                onClick={isDisabled ? null : onSelect}
            >
                {this.props.children}
            </div>
        )
    }
}

Tabs.propTypes = propTypes;

export default Tabs;