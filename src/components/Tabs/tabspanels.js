import React, {Component} from 'react';
import {TabsContext} from "./index";


class TabsPanels extends Component {
    render() {
        return (
            <TabsContext.Consumer>
                {(state) => <div>{this.props.children[state.activeTab]}
                </div>}
            </TabsContext.Consumer>
        )
    }
}

export default TabsPanels;