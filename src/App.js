import React, {Component} from 'react';
import './App.css';
import {AudioPlayer} from "./components/AudioPlayer";
import Play from "./components/AudioPlayer/play";
import Pause from "./components/AudioPlayer/pause";
import ProgressBar from "./components/AudioPlayer/progress";
import mario from "./audio/mariobros.mp3"
import podcast from "./audio/podcast.mp3"
import Next from "./components/AudioPlayer/next";
import Previous from "./components/AudioPlayer/previous";
import FaPlay from 'react-icons/lib/fa/play'
import FaPause from 'react-icons/lib/fa/pause'
import FaForward from 'react-icons/lib/fa/forward'
import FaBackward from 'react-icons/lib/fa/backward'
import {TabsComponent} from "./components/Tabs";
import Tabs from "./components/Tabs/tabs";
import TabsPanels from "./components/Tabs/tabspanels";
import Panel from "./components/Tabs/panel";
import {TabsList} from "./components/Tabs/tabslist"

const songs = [{name: 'mario', data: mario}, {name: 'podcast', data: podcast}];

class App extends Component {
    render() {
        return (
            <div className="App">
                <React.Fragment>
                    <AudioPlayer source={songs} trackNameEnabled playerClassName="player">
                        <Previous buttonClass={'radio-button btn1'}><FaBackward/></Previous>
                        <Play buttonClass={'radio-button btn2'}><FaPlay/></Play>
                        <Pause buttonClass={'radio-button btn3'}><FaPause/></Pause>
                        <Next buttonClass={'radio-button btn4'}><FaForward/></Next>
                        <ProgressBar/>
                    </AudioPlayer>
                    <TabsComponent>
                        <TabsList tabsHeaderClass="tabs-header">
                            <Tabs>FirstTab</Tabs>
                            <Tabs isDisabled>Second Tab</Tabs>
                            <Tabs>Third Tab</Tabs>
                        </TabsList>
                        <TabsPanels>
                            <Panel panelClass="panel"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
                                augue nulla,
                                dapibus vitae sagittis finibus, consequat nec purus. Curabitur vel arcu dictum, lacinia
                                odio nec, tincidunt dui. Nulla auctor quis ligula non vestibulum. Nulla sed mattis
                                tortor. Cras sed commodo lacus, sed dictum diam. In non scelerisque quam, sed luctus
                                dui. Mauris ipsum risus, finibus et finibus vestibulum, dignissim at diam. Integer
                                vestibulum erat ut quam condimentum blandit. In hac habitasse platea dictumst.
                                Donec auctor volutpat nibh, quis
                                rutrum magna.</p></Panel>
                            <Panel panelClass="panel"><p> Nullam euismod, felis a ornare rhoncus, justo urna lobortis
                                odio, tristique
                                malesuada
                                neque nunc sit amet felis. Ut sed magna commodo, ultricies quam vel, placerat arcu.
                                Proin fermentum urna vel felis faucibus, sit amet vehicula augue sagittis. Praesent eu
                                turpis a erat aliquet dictum eget ut felis. Phasellus fringilla purus eu vestibulum
                                congue. Mauris molestie metus sit amet fermentum cursus. Sed viverra quam tortor, vitae
                                gravida augue elementum eu. Phasellus interdum semper nulla, nec placerat diam iaculis
                                sit amet. Nam et sapien tincidunt, scelerisque eros quis, rutrum tellus. In nisl erat,
                                dignissim at risus rutrum, aliquet efficitur diam.</p></Panel>
                            <Panel panelClass="panel"><p>Donec bibendum ligula bibendum ante rutrum ullamcorper.
                                Suspendisse consectetur
                                auctor est, quis fermentum velit gravida in. Nam ultrices risus eget dolor lobortis
                                elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
                                turpis egestas. Cras tristique, quam id maximus sodales, dolor massa ultricies augue,
                                sit amet imperdiet felis sapien in sapien. Sed mollis semper mauris ultrices ornare.
                                Proin ut lacus est.</p></Panel>
                        </TabsPanels>
                    </TabsComponent>
                </React.Fragment>
            </div>
        );
    }
}

export default App;

